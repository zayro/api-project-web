import dotenv = require ('dotenv');

const initOptions = {
    // global event notification;

    connect(client, dc, useCount) {
        const cp = client.connectionParameters;
        console.log('Connected to database:', cp.database);
    },

    query(e) {
        console.log('QUERY -------->:', e.query);
    },

    error: (error: any, e: any) => {
        if (e.cn) {
            // A connection-related error;
            //
            // Connections are reported back with the password hashed,
            // for safe errors logging, without exposing passwords.
            console.log('CN:', e.cn);
            console.log('EVENT:', error.message || error);
        }
    }
};

export const pgp = require('pg-promise')(initOptions);

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: '.env' });

// Preparing the connection details:
 const cn = 'postgres://zayro:zayro@localhost:5432/prueba';
// const cn = `${environment.DBM.engine}://${environment.DBM.user}:${environment.DBM.pass }@${environment.DBM.server}:${environment.DBM.port }/${environment.DBM.db}`;

// Creating a new database instance from the connection details:
export const db = pgp(cn);

// Exporting the database object for shared use:
// module.exports = db;


