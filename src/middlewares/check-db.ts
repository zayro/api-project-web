import { Admin } from '../controllers/admin';

export const checkBdd = async (req, res, next) => {
  try {
    // req.bdd = new Admin('postgres', 'zayro', 'zayro', 'localhost', '5432');
     req.bdd = await new Admin();
    await next();
  } catch (error) {
    return res.status(401).json({
      message: 'Auth failed',
    });
  }
};
