import { User } from '../controllers/user';

export const checkUser = (req, res, next) => {
  try {
    req.user = new User();
    next();
  } catch (error) {
    return res.status(401).json({
      message: 'Auth failed',
    });
  }
};
