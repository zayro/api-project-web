import { Login } from '../controllers/login';

export const checkLogin = (req, res, next) => {
  try {
    req.login = new Login();
    next();
  } catch (error) {
    return res.status(401).json({
      message: 'Auth failed',
    });
  }
};
