import {db, pgp} from '../postgresDB/index';

export class Admin {

    constructor() {

    }

    sql(sql: object) {

        const users =  db.any(sql);
        return users;

    }

    transaction(queries: any[]) {
        db.tx((t: any )=> {

            return t.batch(queries);
        })
            .then(data => {
                // SUCCESS
                // data = array of null-s
            })
            .catch(error => {
                // ERROR
            });
    }

    autoIncrement(table: string, field: string) {

        return  db.any('SELECT MAX($2~) + 1 as $2~ FROM $1~;', [table, field]);
    }

    get(table: string) {

        const users =  db.any('SELECT * FROM $1~;', table);
        return users;

    }


    select(query: string, values: string) {

        return db.any(query, values);

    }

    async insertJson(json: any[], table: string) {

        console.log('----------- JSON -------------', json);

        if (json.length > 1) {

            const KeyName = Object.keys(json);

            return db.any(pgp.helpers.insert(json, KeyName, table));

        } else {

            return db.any(pgp.helpers.insert(json[0], null, table));
        }

    }

    async updateJson(json: any[], table: string) {

        console.log('----------- JSON -------------', json);

        const KeyName = Object.keys(json);

        console.log('----------- SQL -------------', pgp.helpers.update(json, KeyName, table));

        return db.any(pgp.helpers.update(json, KeyName, table));

    }


    insert(sql: string) {

        return db.any(sql);

    }

    update(sql: string) {

        return db.any(sql);

    }

    delete(query: string, values: string) {

        return db.any(query, values);

    }


}
