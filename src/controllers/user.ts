import db from '../postgresDB/index';

export class User {

    add(data: object) {
        return db.one('INSERT INTO users (username, email, password) VALUES(${username}, ${email}, ${password}) RETURNING username, email', data);
    }

    get() {
        return db.any('select username, email from users');
    }

    getUsername() {
        return db.any('select username, email from users where username = ${username}');
    }

    getEmail() {
        return db.any('select username, email from users where email = ${email}');
    }

}
