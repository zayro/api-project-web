import { Login } from './controllers/login';
// set up ======================================================================

// Modules NodeJs
import express = require('express');
import compression = require('compression');  // compresses requests
import dotenv = require ('dotenv');
import path = require ('path');
import bodyParser = require ('body-parser');
import morgan = require ('morgan');


import { Request, Response, Router } from 'express';

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: '.env' });

// Create Express server
const app = express();

app.use(morgan('dev'));
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/uploads', express.static('uploads'));
app.set('port', 3000);


app.use(bodyParser.json());

// Route Access-Control-Allow-Origin
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    return res.status(200).json({});
  }
  next();
  return true;
});

// Routes which should handle requests
// routes ======================================================================
// require('./src/routes/index')(app);
// Module Route
const Route = require('./routes/index');
app.use('/admin', Route.admin);
app.use('/login', Route.login);
app.use('/user', Route.user);





// Route Not Found
app.use((req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

// Route Handle Errors
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
  next(error);
});

// app.listen(3000, () => console.log('Example app listening on port 3000!'))

// module.exports = app;

export default  app ;
