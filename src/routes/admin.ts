// Modules NodeJs
import { Request, Response, Router } from 'express';

import { checkBdd } from '../middlewares/check-db';

import { message } from '../utils/tools';

const jsonSql = require('json-sql')();

const Joi = require('joi');

const colors = require('colors');

const SQLGenerator = require('sql-json-generator');
const sqlGenerator = new SQLGenerator({ pgSQL: true });

const router = Router();

router.post('/sql/', checkBdd, (req: any, res: Response) => {
  const sql = jsonSql.build(req.body);

  req.bdd
    .sql(sql.query)
    .then((data: any) => {
      res.status(200).json(message(true, 'Se ejecuto exitosamente', data));
    })
    .catch((error: any) => {
      console.log('ERROR:', error); // print the error;
      res.status(500).json(message(false, 'ocurrio un problema', error));
    });
});

router.get('/get/:table', checkBdd, (req: any, res: Response) => {
  req.bdd
    .get(req.params.table)
    .then((data: any) => {
      res.status(200).json(message(true, 'Se consulto exitosamente', data));
    })
    .catch((error: any) => {
      console.log('ERROR:', error); // print the error;
      res.status(500).json(message(false, 'ocurrio un problema', error));
    });
});

router.post('/insert/', checkBdd, async (req: any, res: Response) => {
  // Crea Schema Json
  const schema = Joi.object().keys({
    $insert: Joi.string()
      .required(),
    $values: Joi.array()
      .min(1)
      .required()
  });

  // Valida Schema Json
  const result = Joi.validate(req.body, schema);

  if (result.error !== null) {
    res
      .status(500)
      .json(
        message(
          false,
          'ocurrio un problema al Validar Datos',
          result.error.details
        )
      );
  }

  req.bdd
    .insertJson(result.value.$values, result.value.$insert)
    .then((data: any) => {
      res.status(200).json(message(true, 'Se agrego exitosamente', data));
    })
    .catch((error: any) => {
      console.log('ERROR:', error); // print the error;
      res
        .status(500)
        .json(message(false, 'ocurrio un problema al insertar', error));
    });

});


router.post('/insertIncrement/', checkBdd, async (req: any, res: Response) => {
  // Crea Schema Json
  const schema = Joi.object().keys({
    $insert: Joi.string()
      .required(),
    $values: Joi.array()
      .min(1)
      .required(),
    $increment: Joi.string()
  });

  // Valida Schema Json
  const result = Joi.validate(req.body, schema);

  if (result.error !== null) {
    res
      .status(500)
      .json(
        message(
          false,
          'ocurrio un problema al Validar Datos',
          result.error.details
        )
      );
  }


  // Crea Auto-Incrementable
  if (result.value.$increment) {

    const retorno = await req.bdd
    .autoIncrement(result.value.$insert, result.value.$increment)
    .then((response: any) => response);

    // Object.assign(result.value.$values[0], retorno[0]);

    // Recorre los valores
    const bb = await result.value.$values.map(function(
      obj: any[], cont: number = 0
    ) {
      cont++;
      // console.log('------ red -----'.red, retorno[0].idgenero);
      return  Object.assign(obj, retorno[0]);
    });

    await req.bdd
      .insertJson(bb, result.value.$insert)
      .then((data: any) => {
        res.status(200).json(message(true, 'Se agrego exitosamente', data));
      })
      .catch((error: any) => {
        console.log('ERROR:', error); // print the error;
        res
          .status(500)
          .json(message(false, 'ocurrio un problema al insertar', error));
      });

 }

  // req.bdd
  //   .insertJson(result.value.$values, result.value.$insert)
  //   .then((data: any) => {
  //     res.status(200).json(message(true, 'Se agrego exitosamente', data));
  //   })
  //   .catch((error: any) => {
  //     console.log('ERROR:', error); // print the error;
  //     res
  //       .status(500)
  //       .json(message(false, 'ocurrio un problema al insertar', error));
  //   });

});

router.put('/update/', checkBdd, (req: any, res: Response) => {
  // Crea Schema Json
  const schema = Joi.object().keys({
    $update: Joi.string()
      .required(),
    $set: Joi.object()
      .min(1)
      .required(),
    $where: Joi.array()
    .min(1)
    .required()
  });

  // Valida Schema Json
  const result = Joi.validate(req.body, schema);

  if (result.error !== null) {
    res
      .status(500)
      .json(
        message(
          false,
          'ocurrio un problema al Validar Datos',
          result.error.details
        )
      );
  }

  const sql = sqlGenerator.update(result.value);

  req.bdd
    .update(sql)
    .then((data: any) => {
      res.status(200).json(message(true, 'Se actualizo exitosamente', data));
    })
    .catch((error: any) => {
      console.log('ERROR:', error); // print the error;
      res
        .status(500)
        .json(message(false, 'ocurrio un problema al actualizar', error));
    });
});

router.put('/delete/', checkBdd, (req: any, res: Response) => {

  // Crea Schema Json
  const schema = Joi.object().keys({
    $delete: Joi.string()
      .required(),
    $where: Joi.array()
      .min(1)
      .required()
  });

  // Valida Schema Json
  const result = Joi.validate(req.body, schema);

  if (result.error !== null) {
    res
      .status(500)
      .json(
        message(
          false,
          'ocurrio un problema al Validar Datos',
          result.error.details
        )
      );
  }

  const sql = sqlGenerator.delete(result.value);

  req.bdd
    .delete(sql)
    .then((data: any) => {
      res.status(200).json(message(true, 'Se elimino exitosamente al', data));
    })
    .catch((error: any) => {
      console.log('ERROR:', error); // print the error;
      res
        .status(500)
        .json(message(false, 'ocurrio un problema al eliminar', error));
    });
});

export { router as admin };
