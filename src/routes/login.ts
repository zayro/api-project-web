// Modules NodeJs
import { Request, Response, Router } from 'express';

import { checkLogin } from '../middlewares/check-login' ;

import { token, message, compareEncryptedData } from '../utils/tools';

const jsonSql = require('json-sql')();

const router = Router();

router.post('/', checkLogin, (req: any, res: Response) => {

  req.login.login(req.body).then((data: any) => {

    if (compareEncryptedData(req.body.password, data.password)) {
      const response = {
        infoUser: data,
        token: token(data, '1h')
      };
      return res.status(200).json(message(true, 'Autenticacion exitosamente', response));
    } else {
      return  res.status(401).json(message(false, 'Autenticacion fallo'));
    }

   })
   .catch((error: any) => {
     console.log('ERROR:', error); // print the error;
     return res.status(401).json(message(false, 'Email No existe fallo autenticacion', error));
   });

});

export { router as login } ;
