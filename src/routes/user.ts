// Modules NodeJs
import { Request, Response, Router } from 'express';

import { checkUser } from '../middlewares/check-user' ;
import { checkAuth } from '../middlewares/check-auth' ;

import { encrypt, message } from '../utils/tools';

const jsonSql = require('json-sql')();

const router = Router();

router.post('/new', checkUser, (req: any, res: Response) => {

  const send = {
    email: req.body.email,
    username: req.body.username,
    password: encrypt(req.body.password),
    created_at: Date.now()
  };

  req.user.add(send).then((data: any) => {
   res.status(200).json(message(true, 'Se consulto exitosamente', data));
   })
   .catch((error: any) => {
   console.log('ERROR:', error); // print the error;
   res.status(500).json(message(false, 'ocurrio un problema', error));
   });

});

router.get('/', [checkAuth, checkUser], (req: any, res: Response) => {
  req.user.get().then((data: any) => {
   res.status(200).json(message(true, 'Se consulto exitosamente', data));
   })
   .catch((error: any) => {
   console.log('ERROR:', error); // print the error;
   res.status(500).json(message(false, 'ocurrio un problema', error));
   });

});


export { router as user } ;
