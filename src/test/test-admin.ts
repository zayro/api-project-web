// Modules NodeJs
import { Request, Response, Router } from 'express';

import db from '../postgresDB/index';

const router = Router();

function message(status: boolean, response: object, mensaje: string) {
  const data = {};
  data.status = status;
  data.data = response;
  data.message = mensaje;

  return data;
}

router.get('/test/:table/', (req: Request, res: Response) => {

  try {
    const users =  db.any('SELECT * FROM hoteles ');

    users.then(data => {
    console.log('DATA:', data); // print data;
    res.status(200).json(message(true, data, 'Se consulto exitosamente'));

    })
    .catch(error => {
    console.log('ERROR:', error); // print the error;
    });
    // success
} catch (e) {
    // error
}

});



export { router as adminRoutes } ;


