
// Modules NodeJs
import { Request, Response, Router } from 'express';

import { Admin } from '../controllers/admin';

const router = Router();

function message(status: boolean, response: object, mensaje: string) {
  const data = {};
  data.status = status;
  data.data = response;
  data.message = mensaje;

  return data;
}



router.get('/get/', (req: Request, res: Response) => {

    const bdd = new Admin;

    bdd.get().then(data => {
    console.log('DATA:', data); // print data;
    res.status(200).json(message(true, data, 'Se consulto exitosamente'));
    })
    .catch(error => {
    console.log('ERROR:', error); // print the error;
    });

});




export { router as adminRoutes } ;

