import Admin from '../../db/postgresDB/controllers/general';

module.exports = (req, res, next) => {
  try {
    req.bdd = new Admin('postgres', 'zayro', 'zayro', 'localhost', '5432');
    next();
  } catch (error) {
    return res.status(401).json({
      message: 'Auth failed',
    });
  }
};
