// import version es6
import db from '../models/_db';
import model from '../models/producto';

// import version es5
// const model = require('../models/producto');
// model.Producto.sync();

exports.products_get_all = (req, res) => {
  model.Producto.findAll().then((data) => {
    res.status(200).json(data);
  }).catch((err) => {
    res.status(500).json({
      error: err,
    });
  });
};

exports.products_get_id = (req, res) => {
  model.Producto.findAll({
    where: {
      idProducto: req.params.idProducto,
    },
  })
    .then((data) => {
      if (data.length < 1) {
        res.status(404).send({ message: 'Not Found' });
      } else {
        res.status(200).json(data);
      }
    })
    .catch((err) => { res.status(500).json({ error: err }); });
};


exports.products_get_name = (req, res) => {
  model.Producto.findAll({
    where: {
      name: req.params.nameProducto,
    },
  })
    .then((data) => { res.status(200).json(data); })
    .catch((err) => { res.status(500).json({ error: err }); });
};

exports.products_select = (req, res) => {
  db.query('SELECT * FROM producto', { type: db.QueryTypes.SELECT })
    .then((data) => { res.status(200).json(data); })
    .catch((err) => { res.status(500).json({ error: err }); });
};

exports.products_create = (req, res, next) => {
  model.Producto.create({
    name: req.body.name,
  })
    .then((data) => { res.status(201).json(data); })
    .catch((err) => { res.status(500).json({ error: err }); });
};


exports.products_update = (req, res, next) => {
  model.Producto.create({
    name: req.body.name,
  })
    .then((data) => { res.status(201).json(data); })
    .catch((err) => { res.status(500).json({ error: err }); });
};


exports.products_delete = (req, res, next) => {
  model.Producto.create({
    name: req.body.name,
  })
    .then((data) => { res.status(201).json(data); })
    .catch((err) => { res.status(500).json({ error: err }); });
};
